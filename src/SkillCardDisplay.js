import React from 'react';
import "antd/dist/antd.css";
import './App.css';
import { Card} from 'antd';

const {Meta} = Card;


type CardDisplayProps = {
  cards: any
}

function CardDisplay({cards} = CardDisplayProps)  {

  const displayCard = (card) => {
      return (
        <Card style={{width:400, margin: 45}} >
        <Meta
        title={card.title}/>

    {card.description.map((desc) => {return(<div style={{margin:5}}>{desc} <br/></div>)})}
      </Card>
      )
  }

  return (
    <div class = "cardDisplay">
    {cards.map((card)=>displayCard(card))}
    </div>

  )

}



export default CardDisplay;
