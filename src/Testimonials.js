import React from 'react';
import { List} from 'antd';


function Testimonials() {
  const data = [
  {
    description: `During my first year, in my very first semester, my Dean of Studies was trying to dissuade me from pursuing a STEM major (Neuroscience) ....`,
  },


];

  return (
    <List
      itemLayout="horizontal"
      dataSource={data}
      renderItem={item => (
        <List.Item>
          <List.Item.Meta
            description={item.description}
          />
        </List.Item>
      )}
    />

  );
}


export default Testimonials;
