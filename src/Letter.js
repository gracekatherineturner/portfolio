import React from 'react';
import { Space, Layout } from 'antd';
const {Content } = Layout;
function Letter() {
  return (

    <Space direction="vertical" size={5}>
    <Content>
    Dear Dean Poor, Vice Dean Kahn, Associate Deans Bogucki, Yun, Maggard, and Chairs of SEAS departments,
    </Content>
    <Content>
    Following national conversation on anti-black racism and the legacies of 400 years of oppression that manifest in every sphere, we as alumni of the School of Engineering gathered to discuss what can be done to support black students in engineering. As an influential institution of scientific research and learning, SEAS cannot neglect its moral responsibility to combat racism, especially not in this moment of unprecedented reckoning. While progress has been made in increasing student diversity, recent alumnus of the school continue to encounter eerily similar experiences as those who graduated from the program decades ago. If we are to truly resist racism in STEM, we must critically examine our actions and inactions in SEAS and begin to generate creative solutions for current SEAS students in all departments. Considering the disproportionate impact of the covid pandemic on African Americans, we think it’s especially important to keep diversity and inclusion at the forefront of the school’s mind as it prepares to conduct studies fully online.
    </Content>
    <Content>
    We write this letter to urge you to deliberate in good faith on the following demands. We have also included suggested initiatives for each demand under "More Details":
    </Content>
    <Content>
    <Space direction ="vertical" size= {3}>
    <Content>
    1. Increase transparency into the resources and opportunities available in SEAS and the larger engineering industry. Hold outreach events and have greater presence in residential colleges to improve recruitment of BIPOC students. Host department sponsored events with industry leaders that showcase career opportunities available with an engineering degree, make transparent the recruitment process, and expand mentorship programs for academic and professional growth.
    </Content>
    <Content>
    2. Increase funding for groups that specifically benefit BIPOC students at Princeton and the larger Princeton community.
    </Content>
    <Content>
    3. Create a position or fortify existing positions in SEAS that are specifically focused on addressing diversity and inclusion for current students in the undergraduate department. This position would handle discrimination complaints, track retention of BIPOC students (particularly in the first year curriculum), publish statistics, and be empowered to address diversity with specific plans.
    </Content>
    <Content>
    4. Evaluate faculty hiring and graduate admissions process and take concrete steps to improve diversity in both.
    </Content>
    <Content>
    5. Incorporate anti-racist curriculum that highlight contribution of BIPOC and discuss impacts of racism in each field.
    </Content>
    </Space>
    </Content>
    <Content>
    Sincerely,
    </Content>
    <Content>
     SEAS alumni
    </Content>
    </Space>

  );
}


export default Letter;
