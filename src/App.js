import React, { useState } from 'react';
import "antd/dist/antd.css";
import './App.css';
import { Layout, Menu } from 'antd';
import { Space } from 'antd';
import CardDisplay from './CardDisplay.js';
import SkillCardDisplay from './SkillCardDisplay.js';
import Timeline from './Timeline.js';
import sdoh from './static/socialwheel.jpg';
import nlp from './static/nlp.jpeg';
import webdev from './static/webdev.jpg';

const { Header, Content, Footer } = Layout;

const portfolioCards = [
  {title: "The EHR Company: A Prototype", imageSrc: nlp, description: "Uses elasticsearch and other methodologies to encode doctor's notes into machine understandable codes without supervision or human generated rules. In development. Demo Link: https://the-ehr-company.gitlab.io/demo/ Git: https://gitlab.com/the-ehr-company/demo."},
  {title: "Social Determinants of Health", imageSrc: sdoh, description: "Maintained the Social Determinants of Health module for Epic Systems. Decreased domain development and maintenance from a 6 month developer endeavor to a few weeks for an analyst. Improved data standards to allow timeliness reporting."},
  {title: "Princeton SEAS Diversity Initiative", imageSrc: webdev, description: "Built, managed and deployed initiative site. Git:https://gitlab.com/seas-against-racism/seas-diversity-inclusion. Link: https://seas-against-racism.gitlab.io/seas-diversity-inclusion/"},
  {title: "Synopsis: framework for categorizing free text", imageSrc: nlp, description: "Uses simple 'CTRL-F on steroids' algorithm to propogate manually created categorization rules on medium sized datasets (~300-400 entries)."}
]

const skillCards = [
  {title: "Skills", description: ["Languages (Computer): HTML, JS, TS, React, SQL, M, C#, Python, R, etc.","Languages (Human, in descending order of fluency): English, Spanish, Arabic, Japanese"," Epic Systems Certified","Applied Common Sense"]},
  {title: "Education", description: ["Princeton University: Undergraduate in Computer Science (Bachelors of Science and Engineering)","University of Washington: Masters in Bioinformatics (pending)"]},
  {title: "Personal Interests", description: ["Languages (Computer and Human)", "Improving Healthcare Technology and Systems", "Black Belt in Tae Kwon Do and Aikido", "Winter Biking"]},
]

const resume = [
  {title: "University of Washington, Seattle Graduate Student", description: "Graduate studies in natural language processing and EHR (electronic health record) data. Information retrieval on free text doctor's notes.", time: "Fall 2020-"},
  {title: "The EHR Company Developer and Founder", description: "Founded project building a framework that only requires free text inputs (in development) and can then code health data automatically for population and billing purposes. Goal is to create a framework with extensive searching capabilities and no clicks (e.g. to allow doctors to look their patients in the eye). Software Developer, Founder.", time: "Summer 2020-"},
  {title: "Epic Software Engineer", description: "Made social determinants of health (SDOH) accessible through FHIR. Reduced new SDOH content development time from 6 months with software developer to 2 weeks with analyst. Software Developer, Social Determinants of Health Primary Owner.", time: "2018-2020"},
  {title: "Synopsis Developer and Founder", description: "Founded project around making NLP accessible to non-technical healthcare providers. Developed easy to use coding software.", time: "2017-2018"},
  {title: "Children's National Medical Center Developer", description: "Designed privacy protecting category analytic software. Built a end-to-end web application for hospital staff. Researched patient admission delays. Summer Intern.", time: "2016 and 2017 Summer"},
  {title: "Suicide Hotline Researcher", description: "Analyzed demographic & linguistic patterns in hotline chats. Principal Investigator.", time: "2015 Summer"},
  {title: "Princeton University Undergraduate", description: "Bachelor of Science and Engineering: Computer Science", time: "2014-2018"}
]

function App() {

  const [visibleContent,setVisibleContent] = useState("1")

  return (
    <Layout>
      <Header style={{ position: 'fixed', zIndex: 1, width: '100%'}}>
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
          <Menu.Item key="0" disabled ><div style={{color:"white"}}>Grace Katherine Turner</div></Menu.Item>
          <Menu.Item key="1" onClick = {() => setVisibleContent("1")}>Portfolio</Menu.Item>
          <Menu.Item key="2" onClick = {() => setVisibleContent("2")}>Resume and CV </Menu.Item>

        </Menu>
      </Header>


      <Content id="1" className="site-layout" style={{ padding: '0px 40px', marginTop: 64 }}>
        <div className="site-layout-background" style={{ padding: 24, minHeight: 500}}>
        {visibleContent==="1" && <CardDisplay cards = {portfolioCards}/>}
        {visibleContent==="2" && <Space align="start" direction= "horizontal"><SkillCardDisplay cards={skillCards} /> <Timeline timepoints = {resume}/> </Space>}
        </div>
      </Content>}
      <Footer style={{ textAlign: 'center' }}>
      <Space size={5}>
        <h2> Grace Katherine Turner </h2>
      </Space>
      </Footer>
    </Layout>
  );
}

export default App;
