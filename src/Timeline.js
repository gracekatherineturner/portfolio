import React from 'react';
import "antd/dist/antd.css";
import './App.css';
import { Timeline } from 'antd';


type TimelineProps = {
  timepoints: any
}

function BasicTimeline({timepoints} = TimelineProps) {

  return (
    <div class ="">
    <br/>
    <br/>
    <Timeline mode ="left">
    {timepoints.map((time) => {return (<Timeline.Item label={time.time}>
    <b>{time.title}</b><br/>
    {time.description}
    </Timeline.Item>
    )})}
    </Timeline>
    </div>
  )
}


export default BasicTimeline;
