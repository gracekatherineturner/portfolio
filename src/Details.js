import React from 'react';
import { List, Layout,Space } from 'antd';
const {Content } = Layout;

function Details() {
  const data = [
  {
    title: 'Increase transparency in the resources and opportunities available.',
    description: [`1. Provide a consolidated list of available guidance and resources in SEAS to all students. Make sure these resources, such as the undergraduate handbook for each department, are particularly made known to first year students who have not yet chosen a department.`,

`2. Improve recruitment and retention of under-represented undergraduates into SEAS, especially during the difficult early years.`,
`3. Increase outreach to students, such as an event inviting the student body emphasizing that anyone can do engineering. This could include expanding the SEAS open house, and reaching out to students not already committed to engineering by using residential college resources.`,
`4. Expand formal, small-group mentorship programs to increase actual contact with interested students. Compensation and increased space and resources to mentors could increase the quality of the mentorship.`,
`5. Mentorship should be offered on course planning and department selection, as well as career planning and professional opportunities.`,
`6. Host department sponsored events with industry leaders that showcase career opportunities available with an engineering degree.`
],
  },
  {
    title: 'Increase funding for groups that specifically benefit BIPOC students.',
    description: [`1. Increase available funding for organizations such as NSBE that provide critical support to BIPOC students and their career development.`,
`2. Make transparent the funding that is distributed by SEAS to various student groups.`,
`3. Increase outreach to the community, particularly in under-resourced areas.`,
`4. Provide funding for students and faculty to perform engineering demos and lessons in under-resourced elementary, middle, and high schools both in Princeton and in Newark and Trenton communities.`,
`Examples include:`,
` -Princeton Center for Complex Materials’ Dias de las Ciencias`,
` -Princeton Plasma Physics Laboratory’s Young Women’s Conference`,
`5. Increase outreach opportunities for students to help underprivileged high school students with their college applications, and college students with their graduate school applications.`,
`6. Establish fully funded research and learning programs for BIPOC at the high school and undergraduate level.`,
`Examples include:`,
` -Chemistry Department - Summer Undergraduate Research Program: Catalyzing Diversity in Chemistry Leadership`,
` -Princeton Center for Complex Materials - Princeton University Materials Academy (PUMA)`
],
  },
  {
    title: 'Create or fortify a position in SEAS specifically focused on addressing diversity and inclusion.',
    description: [`If such a position already exists, their objectives and initiatives should be made transparent and widely-accessible to the student body. We think this position should consider implementing the following:`,

`1. Publish statistics on class and faculty demographics in each of the engineering department websites to increase transparency in SEAS.`,
`2. Investigate the demographics of students who switch out of SEAS majors and drop out of SEAS classes--these can help indicate where and when students are forced out by unwelcoming environments. For all underrepresented students who churn out of SEAS, conduct a proper exit interview that is reviewed by the SEAS board. If a pattern to the churn is discovered, the SEAS administration is accountable for any mitigation(s).`,
`3. Implement a code of conduct that covers exclusionary behavior. Investigate and address code of conduct issues as they occur.`,
`4. Create (or more widely publicize, if one exists) a complaint box website where students can anonymously submit their experience (either in general or with a specific professor, etc) around violations of the code of conduct.`,
` -Be very clear about what the accountability process here - these complaints should not go into a void, so it is important that it is explicitly someone’s job to act in response to them.`,
` -Consider sending these complaints to an outside, unbiased third party for response.`,
`5. Implement a climate survey every year that is reviewed by the outside consultant, hold town halls at least every year, if not every semester discussing the results, and actually respond to issues raised in these surveys.`,
`6. Create and fund a rotating SEAS board including current students and alumni to hold the school accountable to longer-term change and improving culture. The SEAS board would work with the SEAS administration to amplify the voices of underrepresented SEAS students and examine the ways the SEAS culture and imbalanced power structures contribute to disproportionate experiences.`,
],
  },
  {
    title: 'Evaluate faculty hiring and graduate admissions process and improve the diversity in both.',
    description: [
`1. Increase transparency about applicant pool diversity.`,
`2. Make sure fee-waivers are easily accessible. SEAS could grant a set amount of waivers per department.`,
`3. Implement or increase graduate fellowships for students from underrepresented groups.`,
`4. Remove all GRE requirements, not just this year due to COVID, but permanently. A study at UNC Chapel Hill’s biomedical graduate program showed that the test was not predictive of student publications or PhD completion time. The GRE is a burden on applicants (over $200 per exam), and many programs have cut it from their requirements.`,
`5. Require bias training for those making admissions decisions.`,
`6. Increase student participation in admissions.`,
`7. Implement a rubric for admissions.`,
`8. Lack of diversity in the applicant pool is not a sufficient excuse. For example, are we putting in effort to recruit from HBCUs? Are faculty giving seminars at minority-serving institutions or attending minority-focused conferences?`,
`9. Implement a grant opportunity for students to attend these conferences at no personal cost.`,
`10. In the admissions process, how much weight is given to the so-called “prestige” of the student’s undergraduate institution?`,
`11. Preliminary visit/recruitment day in fall before applications are due for BIPOC/underrepresented students.`,
`12. Recommend an external audit on admissions processes (including faculty hiring).`,
`13. Require a diversity statement in all graduate applications.`,
`14. Dedicate a day during graduate visit weekends for BIPOC.`,
`15. Increase diversity of tenure-track faculty, lecturers, and visiting professors.`,
` -Departments should set and stick to measurable goals.`,
` -Many above ideas about graduate recruitment apply here. Make sure to recruit from a wide variety of universities, recruit at minority-serving conferences, anti-bias training for committee members, hiring rubric to combat bias (and on that rubric, how much weight are we giving the so-called “prestige” of the undergraduate and graduate institutions? What biases does this reflect?).`,
` -Include an elected graduate student representative involved in the hiring process that is bias trained, as is done at other universities.`,
` -During interview day, include opportunities for candidates to discuss commitment to DEI.`
],
  },

];

  return (
    <List
      itemLayout="horizontal"
      dataSource={data}
      renderItem={item => (
        <List.Item>
          <List.Item.Meta
            title={item.title}
            description={<Space direction="vertical" size={3}>{item.description.map((desc)=> {return(<Content>{desc}</Content>)})}</Space>}
          />
        </List.Item>
      )}
    />

  );
}


export default Details;
